#!/usr/bin/python

from multi_lists import *
from gibbs_diagram import *

def read_plot(filename, d_grid=100, dx=1.0):
    """Reads data from binary file and plot diffusion paths"""
    f = file(filename,'rb')
    n1_beta = np.load(f)
    n2_beta = np.load(f)
    n1_alpha = np.load(f) 
    n2_alpha = np.load(f)
    n1 = np.load(f)
    n2 = np.load(f)
    f.close()

    x_axis = np.linspace(0,1, num=d_grid)
    fig = plt.figure()
    fig1 = fig.add_subplot(2,1,1)
    fig1.plot(x_axis,n1)  
    fig2 = fig.add_subplot(2,1,2)
    fig2.plot(x_axis,n2)
    fig.savefig(filename[:-4] + '.jpg')  
    plt.close(fig)

    gibbs_diagram_paths(n1_beta,n2_beta, 'beta_diag_' + filename[:-4], 'p')    
    plt.close()
    return 0

read_plot('mu_final2_nonadapt.bin')
