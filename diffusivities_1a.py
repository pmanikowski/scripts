#usr/bin/python

from numpy import linspace, array, exp
from matplotlib import pyplot

def diffusion_plot(temperatures, densities, 
                    diffusion_coefficients_list, title, option='save'):
    """Plot Ni-Pt diffusivities in range of temperatures"""
    markers = ['p', '*', 'd', '^', '8']
    for diffusion_coefficient in diffusion_coefficients_list:
        pyplot.plot(densities, diffusion_coefficient , label=
         str(temperatures[diffusion_coefficients_list.index(
            diffusion_coefficient)]), marker=markers[
                diffusion_coefficients_list.index(diffusion_coefficient)])    
    pyplot.legend(loc='upper left')
    pyplot.title(title)    
    if option == 'save':
        pyplot.savefig(title)
    else:
        pyplot.show()
    pyplot.close()
    return True

def calculate_diffusivities(temperatures, ni, pt):
    """Calculate diffusivities from Gong data"""
    r = 8.31 
    delta_g_pt = []
    delta_g_ni = [] 
    d_pt_list = []
    d_ni_list = []
    d_interdiff_list = []

    # Mobilities of Ni
    g_Ni_Ni = lambda x: -271377.6 - 81.79 * x 
    g_Ni_Pt = lambda x: -350108.6 - 33.18 * x 
    g_Ni_PtNi_0 = lambda x: 281047.4 - 199.07 * x 
    # Moblities of Pt
    g_Pt_Pt = lambda x: -261426.9 - 99.17 * x 
    g_Pt_Ni = lambda x: -290813.6 - 78.97 * x 
    g_Pt_PtNi_0 = lambda x: 302606.4 - 192.87 * x 

    for temp in temperatures:
        ## Diffusivities in um2/s
        delta_g_pt.append([pt[i]*g_Pt_Pt(temp) + ni[i]*g_Pt_Ni(temp) + 
                pt[i]*ni[i]*g_Pt_PtNi_0(temp) for i in xrange(grid+1)])
        delta_g_ni.append([pt[i]*g_Ni_Pt(temp) + ni[i]*g_Ni_Ni(temp) + 
                pt[i]*ni[i]*g_Ni_PtNi_0(temp) for i in xrange(grid+1)])
        d_pt_list.append([1e12 * exp(g/(r*temp)) for g in delta_g_pt[-1]]) 
        d_ni_list.append([1e12 * exp(g/(r*temp)) for g in delta_g_ni[-1]])
        d_interdiff_list.append([ni[i]*d_pt_list[-1][i] + 
                    pt[i]*d_ni_list[-1][i] for i in xrange(grid+1)])
    return d_pt_list, d_ni_list, d_interdiff_list

#Data
grid = 20
temperatures = [1273, 1323, 1373, 1423, 1473]
ni = linspace(0.0, 1.0, num = grid+1)  
pt = ni[::-1]

d_pt, d_ni, d_interdiff = calculate_diffusivities(temperatures, ni, pt)
diffusion_plot(temperatures, ni, d_ni, 'D_Ni')
diffusion_plot(temperatures, pt, d_pt, 'D_Pt')
diffusion_plot(temperatures, ni, d_interdiff, 'D_Interdiffusion', "show")
