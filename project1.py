"""Project 1: Degree Distributions for graphs"""

# Testing graphs
EX_GRAPH0 = {0:set([1,2]), 1:set([]), 2:set([])}
EX_GRAPH1 = {0:set([4,5,1]),1:set([2,6]),2:set([3]),3:set([0]),4:set([1]),5:set([2]), 6:set([])}
EX_GRAPH2 = {0:set([4,5,1]),1:set([2,6]),2:set([3,7]),3:set([7]),4:set([1]),5:set([2]),6:set([]),7:set([3]),8:set([1,2]),9:set([0,4,5,6,7,3])}

def make_complete_graph(num_nodes):
    ''' Creates complete graph with all nodes are connected '''
    if num_nodes<=0:
        return {}
    graph = {}
    for node in xrange(num_nodes):
        neighbours = [no for no in xrange(num_nodes) if no!=node]
        graph[node]=set(neighbours)
    return graph

def compute_in_degrees(digraph):
    '''Computes in-degrees of directed graph '''
    degrees = {}
    dummy_items = list(digraph.values()[0])+digraph.keys()
    listed_items = []
    for dummy in dummy_items:
        if dummy not in listed_items:
            listed_items.append(dummy)
    for item in listed_items:
        t_degree= [i for sublist in digraph.values() for i in sublist].count(item)
        degrees.update({item:t_degree})
    return degrees

def in_degree_distribution(digraph):
    '''Calculates in-degree distribution'''
    in_degrees = {}
    degrees = compute_in_degrees(digraph)
    for val in degrees.values():
        if val not in in_degrees:
            in_degrees[val]=degrees.values().count(val)
    return in_degrees 
    
def in_degree_norm(digraph):
    '''Normalize in-degree distribution'''
    distr = in_degree_distribution(digraph)
    normed_distr = {}
    sum_distr = sum(distr.values())
    print sum_distr
    for key, val in distr.items():
        normed_distr[key]=float(val)/sum_distr
    return normed_distr

