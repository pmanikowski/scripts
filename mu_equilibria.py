#!/usr/bin/python
from multi_lists import *

def multi_equilibria():
    """Multi-multi model solver with zero entropy production. Euler method"""
    #Number of grid points    
    d_grid = 100
    #Spatial step size
    dx = 1e-3
    a_interval = x_interval
    #Time step size
    dt = 8.0
    #Initial time
    t = 0.0 
    #Total time of the process     
    t_total = 36000
    #Diffusivities of elements in phases [1e-12 cm2/s]    
    d1a = 2e-11
    d2a = 1e-12
    d3a = 1e-11
    d1b = 6e-11
    d2b = 5e-12
    d3b = 1e-11
    #Initial concentrations of diffusion couple
    n1_init_left = 0.1
    n1_init_right = 0.3
    n2_init_left = 0.5
    n2_init_right = 0.45
    n1 = [d_grid/2*[n1_init_left] + d_grid/2*[n1_init_right]]     
    n2 = [d_grid/2*[n2_init_left] + d_grid/2*[n2_init_right]]
    while t<t_total:   
        #list of equilibrium values for all nodes
        equilibria = []
        n1_temp = []
        n2_temp = []
        n1_temp.append(n1_init_left)
        n2_temp.append(n2_init_left)
        for xg in xrange(0,d_grid):
            equilibria.append(get_equilibria(n1[0][xg], n2[0][xg]))
            
        for x in xrange(1,d_grid-1):
            n1al = equilibria[x-1][1][1]
            n1bl = equilibria[x-1][1][0]
            n1a = equilibria[x][1][1]
            n1b = equilibria[x][1][0]
            n1ar = equilibria[x+1][1][1]        
            n1br = equilibria[x+1][1][0]
            tie_line = equilibria[x][1][2]
            m_alpha = (a_boundary(n1_alpha[equilibria[x][0]+1])-a_boundary(n1_alpha[equilibria[x][0]]))/a_interval
            dm_alpha = (((a_boundary(n1_alpha[equilibria[x][0]+1])-a_boundary(n1_alpha[equilibria[x][0]]))/a_interval)- ((a_boundary(n1_alpha[equilibria[x][0]])-a_boundary(n1_alpha[equilibria[x][0]-1]))/a_interval))/a_interval  
            m_beta = (b_boundary(n1_beta[equilibria[x][0]+1])-b_boundary(n1_beta[equilibria[x][0]]))/beta_interval
            dm_beta = (((b_boundary(n1_beta[equilibria[x][0]+1])-b_boundary(n1_beta[equilibria[x][0]]))/beta_interval)- ((b_boundary(n1_beta[equilibria[x][0]])-b_boundary(n1_beta[equilibria[x][0]-1]))/beta_interval))/beta_interval          
            #Tilda diffusivities    
            diff1_ta = (-d1a + n1a*(d1a-d3a) + n1a*m_alpha*(d2a-d3a))         
            diff1_tb = (-d1b + n1b*(d1b-d3b) + n1b*m_beta*(d2b-d3b))           
#            #Fluxes     
            j1a = diff1_ta * 0.5/dx * (n1ar-n1al)  
            j1b = diff1_tb * 0.5/dx * (n1br-n1bl)
#            #Fluxes derivatives over x
            d_j1a = diff1_ta * 1/dx**2 *(n1ar+n1al-2*n1a) + (0.5/dx *(n1ar-n1al))**2 *(d1a-d3a + (d2a-d3a)*(m_alpha + n1a*dm_alpha))
            d_j1b = diff1_tb * 1/dx**2 *(n1br+n1bl-2*n1b) + (0.5/dx *(n1br-n1bl))**2 *(d1b-d3b + (d2b-d3b)*(m_beta + n1b*dm_beta)) 
#            #Fraction of beta phase from lever rule
            fib = (n1a-n1[0][x])/(n1a-n1b)
            d_fib = ( 0.5/dx * (n1[0][x+1]-n1[0][x-1]-n1ar+n1al) * (n1b-n1a) -  0.5/dx * (n1br-n1bl-n1ar+n1al) * (n1[0][x]-n1a) ) / (n1b-n1a)**2
            
            n1_temp.append(n1[0][x] - dt * (d_j1a + fib*(d_j1b-d_j1a) + d_fib*(j1b-j1a))) 

            dm_tie_a = 0.5*(m_tie[equilibria[x][0]+1] - m_tie[equilibria[x][0]-1])/a_interval
            dm_tie_b = 0.5*(m_tie[equilibria[x][0]+1] - m_tie[equilibria[x][0]-1])/beta_interval
 
            if n1[0][x+1]-n1[0][x-1] != 0 and  n1ar-n1al != 0 :
                dn2_dn1a = m_alpha + dm_tie_a*(n1[0][x]-n1a) + tie_line * ( n1[0][x+1]-n1[0][x-1] )/( n1ar-n1al )     
                dn2_dn1b = m_beta + dm_tie_b*(n1[0][x]-n1a) + tie_line * ( n1[0][x+1]-n1[0][x-1] )/( n1br-n1bl ) 
                dn1a_dn1 = (n1ar-n1al)/(n1[0][x+1]-n1[0][x-1])
                dn1b_dn1 = (n1br-n1bl)/(n1[0][x+1]-n1[0][x-1])
                dn2_dn1 = dn2_dn1a * dn1a_dn1 + dn2_dn1b * dn1b_dn1
                n2_temp.append( n2[0][x] + dn2_dn1 * (n1_temp[x]-n1[0][x]) )                        
            else:
                n2_temp.append(n2[0][x])  
        if t%10 ==0:
            print t
        n1_temp.append(n1_init_right)
        n2_temp.append(n2_init_right)
        n1.append(n1_temp)
        n2.append(n2_temp)
        del n1[0]
        del n2[0]    
        t+=dt
    output = file('multi_equlibria.bin', 'w')
    np.save(output, n1[0])
    np.save(output, n2[0])    
    output.close()
  
    return None   
