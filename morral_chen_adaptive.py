#!/usr/bin/python
from multi_lists import *

def morral_chen():
    """Morral-Chen zig-zag model with varying effective diffusion matrix. Heuns method with adaptive time-step"""
    #Number of grid points    
    d_grid = 100 
    # x step
    dx = 1.0
    # initial time
    t = 0.0
    # time step
    dt = 1.0
    #Number of time steps     
    total_time = 36000
    #Tolerance 
    tol = 1e-5
    #Diffusivities 
    d11 = 1e-3
    d12 = 1e-3
    d21 = 1e-4
    d22 = 1e-4

    #Initial concentrations of diffusion couple
    n1_init_left = 0.1
    n1_init_right = 0.3
    n2_init_left = 0.5
    n2_init_right = 0.45

    n1 = np.array(d_grid/2 * [n1_init_left] + d_grid/2 * [n1_init_right])
    n2 = np.array(d_grid/2 * [n2_init_left] + d_grid/2 * [n2_init_right])
    n1_init = np.copy(n1)
    n2_init = np.copy(n2)
    
    while t<total_time:
        n1_euler = np.zeros(d_grid)
        n2_euler = np.zeros(d_grid)
        n1_heuns = np.zeros(d_grid)
        n2_heuns = np.zeros(d_grid)
        while True:
            try:
                #Neumann-type boundary condition (no flux) Euler left-hand side 
                i,j = get_equilibria(n1[0], n2[0])
                e = 1/beta_interval * (m_tie[i+1]-m_tie[i])
                m_beta = 1/beta_interval * (b_boundary(j[0]+beta_interval)-b_boundary(j[0]))
                dn1b_dn1 = -j[2]/(m_beta-j[2]+e*(n1[0]-j[0]))
                dn2b_dn1 = m_beta*dn1b_dn1
                dn1b_dn2 = 1/(m_beta-j[2]+e*(n1[0]-j[0]))
                dn2b_dn2 = m_beta*dn1b_dn2
                multiplier1_left = d11*dn1b_dn1 + d12*dn2b_dn1
                multiplier2_left = d11*dn1b_dn2 + d12*dn2b_dn2
                multiplier3_left = d21*dn1b_dn1 + d22*dn2b_dn1
                multiplier4_left = d21*dn1b_dn2 + d22*dn2b_dn2

                n1_euler[0] = n1[0] + multiplier1_left * dt/dx**2 * (n1[1]-n1[0]) + multiplier2_left * dt/dx**2 * (n2[1]-n2[0])     
                n2_euler[0] = n2[0] + multiplier3_left * dt/dx**2 * (n1[1]-n1[0]) + multiplier4_left * dt/dx**2 * (n2[1]-n2[0])   

                #Internal grid points evaluation - Euler 
                for x in xrange(1,d_grid-1):
                    i,j = get_equilibria(n1[x], n2[x])
                    e = 1/beta_interval * (m_tie[i+1]-m_tie[i])
                    m_beta = 1/beta_interval * (b_boundary(j[0]+beta_interval)-b_boundary(j[0]))
                    dn1b_dn1 = -j[2]/(m_beta-j[2]+e*(n1[x]-j[0]))
                    dn2b_dn1 = m_beta*dn1b_dn1
                    dn1b_dn2 = 1/(m_beta-j[2]+e*(n1[x]-j[0]))
                    dn2b_dn2 = m_beta*dn1b_dn2
                    multiplier1 = d11*dn1b_dn1 + d12*dn2b_dn1
                    multiplier2 = d11*dn1b_dn2 + d12*dn2b_dn2
                    multiplier3 = d21*dn1b_dn1 + d22*dn2b_dn1
                    multiplier4 = d21*dn1b_dn2 + d22*dn2b_dn2
                    n1_euler[x] = n1[x] + multiplier1 * dt/dx**2 * (n1[x+1]-2*n1[x]+n1[x-1]) + multiplier2 * dt/dx**2 * (n2[x+1]-2*n2[x]+n2[x-1])     
                    n2_euler[x] = n2[x] + multiplier3 * dt/dx**2 * (n1[x+1]-2*n1[x]+n1[x-1]) + multiplier4 * dt/dx**2 * (n2[x+1]-2*n2[x]+n2[x-1])                      

                #Neumann-type boundary condition (no flux) Euler right-hand side
                i,j = get_equilibria(n1[d_grid-1], n2[d_grid-1])
                e = 1/beta_interval * (m_tie[i+1]-m_tie[i])
                m_beta = 1/beta_interval * (b_boundary(j[0]+beta_interval)-b_boundary(j[0]))
                dn1b_dn1 = -j[2]/(m_beta-j[2]+e*(n1[d_grid-1]-j[0]))
                dn2b_dn1 = m_beta*dn1b_dn1
                dn1b_dn2 = 1/(m_beta-j[2]+e*(n1[d_grid-1]-j[0]))
                dn2b_dn2 = m_beta*dn1b_dn2
                multiplier1_right = d11*dn1b_dn1 + d12*dn2b_dn1
                multiplier2_right = d11*dn1b_dn2 + d12*dn2b_dn2
                multiplier3_right = d21*dn1b_dn1 + d22*dn2b_dn1
                multiplier4_right = d21*dn1b_dn2 + d22*dn2b_dn2
                n1_euler[d_grid-1] = n1[d_grid-1] + multiplier1_right * dt/dx**2 * (n1[d_grid-2]-n1[d_grid-1]) + multiplier2_right * dt/dx**2 * (n2[d_grid-2]-n2[d_grid-1])     
                n2_euler[d_grid-1] = n2[d_grid-1] + multiplier3_right * dt/dx**2 * (n1[d_grid-2]-n1[d_grid-1]) + multiplier4_right * dt/dx**2 * (n2[d_grid-2]-n2[d_grid-1])  

                #Neumann-type boundary condition (no flux) Heuns left-hand side
                dn1_euler = n1[1] - n1[0]
                dn2_euler = n2[1] - n2[0]
                dn1_heuns = n1_euler[1] - n1_euler[0]
                dn2_heuns = n2_euler[1] - n2_euler[0]
                n1_heuns[0] = n1[0] + multiplier1_left * dt/dx**2 * 0.5 * (dn1_euler+dn1_heuns) + multiplier2_left * dt/dx**2 * 0.5 * (dn2_euler+dn2_heuns)     
                n2_heuns[0] = n2[0] + multiplier3_left * dt/dx**2 * 0.5 * (dn1_euler+dn1_heuns) + multiplier4_left * dt/dx**2 * 0.5 * (dn2_euler+dn2_heuns)

                #Internal grid points evaluation - Heuns
                for x in xrange(1,d_grid-1):
                    i,j = get_equilibria(n1[x], n2[x])
                    e = 1/beta_interval * (m_tie[i+1]-m_tie[i])
                    m_beta = 1/beta_interval * (b_boundary(j[0]+beta_interval)-b_boundary(j[0]))
                    dn1b_dn1 = -j[2]/(m_beta-j[2]+e*(n1[x]-j[0]))
                    dn2b_dn1 = m_beta*dn1b_dn1
                    dn1b_dn2 = 1/(m_beta-j[2]+e*(n1[x]-j[0]))
                    dn2b_dn2 = m_beta*dn1b_dn2
                    multiplier1 = d11*dn1b_dn1 + d12*dn2b_dn1
                    multiplier2 = d11*dn1b_dn2 + d12*dn2b_dn2
                    multiplier3 = d21*dn1b_dn1 + d22*dn2b_dn1
                    multiplier4 = d21*dn1b_dn2 + d22*dn2b_dn2
                    dn1_euler = n1[x+1] - 2*n1[x] + n1[x-1]
                    dn2_euler = n2[x+1] - 2*n2[x] + n2[x-1]
                    dn1_heuns = n1_euler[x+1] - 2*n1_euler[x] + n1_euler[x-1]
                    dn2_heuns = n2_euler[x+1] - 2*n2_euler[x] + n2_euler[x-1]
                    n1_heuns[x] = n1[x] + multiplier1 * dt/dx**2 * 0.5 * (dn1_euler+dn1_heuns) + multiplier2 * dt/dx**2 * (dn2_euler+dn2_heuns)     
                    n2_heuns[x] = n2[x] + multiplier3 * dt/dx**2 * 0.5 * (dn1_euler+dn1_heuns) + multiplier4 * dt/dx**2 * (dn2_euler+dn2_heuns)                  
              
                #Neumann-type boundary condition (no flux) Heuns right-hand side
                dn1_euler = n1[d_grid-2] - n1[d_grid-1]
                dn2_euler = n2[d_grid-2] - n2[d_grid-1]
                dn1_heuns = n1_euler[d_grid-2] - n1_euler[d_grid-1]
                dn2_heuns = n2_euler[d_grid-2] - n2_euler[d_grid-1]
                n1_heuns[d_grid-1] = n1[d_grid-1] + multiplier1_right * dt/dx**2 * 0.5 * (dn1_euler+dn1_heuns) + multiplier2_right * dt/dx**2 * 0.5 * (dn2_euler+dn2_heuns)     
                n2_heuns[d_grid-1] = n2[d_grid-1] + multiplier3_right * dt/dx**2 * 0.5 * (dn1_euler+dn1_heuns) + multiplier4_right * dt/dx**2 * 0.5 * (dn2_euler+dn2_heuns)
                
                lte = max(abs(np.append(n1_heuns-n1_euler,n2_heuns-n2_euler)))                     
                if lte > tol:
                    dt *=  0.9 * np.sqrt(tol/lte)                    
                else:

                    break

            except TypeError:
                    print 'Path crossing phase boundary at step: ', t
                    n1k = np.copy(n1)
                    n2k = np.copy(n2)                
    #                gibbs_diag(n1k,n2k, True)
                    output = file('broken_morral_chen.bin', 'w')
                    np.save(output, n1)
                    np.save(output, n2)
                    output.close()
                    return None

        n1 = np.copy(n1_heuns)
        n2 = np.copy(n2_heuns)
        t+=dt
        dt *= np.sqrt(tol/lte)
#        print 'dt: ', dt
        print 't: ', t

    output = file('morral_chen.bin', 'w')
    np.save(output, n1)
    np.save(output, n2)
    output.close()

    return True
        

