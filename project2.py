"""Project2: Connected components and graph resilience"""
from collections import deque
from random import choice

# Testing graphs
EX_GRAPH0 = {0:set([1,2]), 1:set([0,2,6]), 2:set([0,1,6,7,8]), 3:set([4,5]), 4:set([3,5]), 5:set([3,4]), 6:set([1,2]), 7:set([2]), 8:set([2])}
EX_GRAPH1 = {0:set([4,5,1,3]),1:set([2,6,0,4,8]),2:set([3,1,5]),3:set([0,2]),4:set([1,0]),5:set([2,0]), 6:set([1]), 8:set([1])}
             
def bfs_visited(ugraph, start_node):
    """returns nodes connected with a given node"""
    queue = deque()
    visited = set([start_node])
    queue.append(start_node)
    while queue!=[]:
        try:
            j_element = queue.popleft()
        except IndexError:
            break
        for key in ugraph.get(j_element):
            if key not in visited:
                visited.add(key)
                queue.append(key)
    return visited

def cc_visited(ugraph):
    """returns list of connected sets of nodes"""
    remaining_nodes = ugraph.keys()
    connected_components = []
    while remaining_nodes!=[]:
        random_node = choice(remaining_nodes)
        visited = set(bfs_visited(ugraph, random_node))
        connected_components.append(visited)
        remaining_nodes=[i for i in remaining_nodes if i not in visited]
    return connected_components

def largest_cc_size(ugraph):
    """returns the most connected node"""
    components = cc_visited(ugraph)
    set_length = 0
    for component in components:
        if len(component)>set_length:
            set_length = len(component)
    return set_length

def compute_resilience(ugraph, attack_order):
    """returns largest node after removal of node from attack_order list"""
    largest_cc = []
    largest_cc.append(largest_cc_size(ugraph))
    for node in attack_order:
        try:
            connexions = ugraph[node]
            del ugraph[node]
            for neighbour in connexions:
                ugraph[neighbour].remove(node)
            largest_cc.append(largest_cc_size(ugraph))
        except IndexError:
            pass
    return largest_cc


    
