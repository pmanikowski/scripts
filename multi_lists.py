#!/usr/bin/python

"""3 phase diagram with 2-phase zone, phase boundaries, tie lines, diffusion paths"""
import matplotlib.pyplot as plt
import numpy as np
import math

#number of datapoints on diagram
grid = 1000
#grid interval
x_interval = 1.0/grid
x_interval_sqrt = x_interval**2
#number of tie lines
number_tie = 1000

#bottom dataset for a diagram
x = np.linspace(0.0,1.0, num=grid+1)
#hypotenuse list
y_roof = [-i+1 for i in x]

#beta linear phase boundary 
beta_slope = 0.15
beta_x0 = 0.1
beta_bound = [beta_x0+beta_slope*i for i in x if beta_x0+beta_slope*i<-i+1.0]
x_beta_max = max([i for i in x if beta_x0+beta_slope*i<-i+1.0])
x_beta_index = int(grid*x_beta_max)
# print x_beta_max

#alpha quadratic phase boundary
alpha_bottom = 0.3
alpha_x0 = 0.6
alpha_bound = [alpha_x0+(i-alpha_bottom)**2 for i in x if alpha_x0+(i-alpha_bottom)**2<-i+1.0]
x_alpha_max = max([i for i in x if alpha_x0+(i-alpha_bottom)**2<-i+1.0])
x_alpha_index = int(grid*x_alpha_max) 
# print x_alpha_max

#supplementary functions of phase boundaries
a_boundary = lambda x: (x-alpha_bottom)**2 + alpha_x0
b_boundary = lambda x: beta_x0+beta_slope*x

#beta phase boundary length
beta_length = math.sqrt((beta_x0-b_boundary(x_beta_max))**2+x_beta_max**2)

#alpha phase boundary length
i = 0
dy_alpha = []
while True:
    try:
        dy_alpha.append(alpha_bound[i+1]-alpha_bound[i])
        i+=1
    except IndexError:
        break

alpha_arcs = [math.sqrt(dy**2+x_interval_sqrt) for dy in dy_alpha]
alpha_length = sum(alpha_arcs)
arc_min = alpha_arcs.index(min(alpha_arcs))

#intervals between tie lines on alpha boundary
alpha_interval = alpha_length/(number_tie+1)
print 'alpha_interval', alpha_interval

arc_temp = 0.
#indexes of x for tie lines on alpha phase boundary 
tie_points = []
for i in alpha_arcs[:arc_min]:
    arc_temp+=i
    if arc_temp>alpha_interval:
        tie_points.append(alpha_arcs.index(i))
        arc_temp = 0.
for j in alpha_arcs[arc_min:]:
    arc_temp+=j
    if arc_temp>alpha_interval:
        tie_points.append(alpha_arcs[arc_min:].index(j)+arc_min)
        arc_temp = 0.
        
a_bound_x = [x_interval*i for i in tie_points]
a_bound_y = [a_boundary(i) for i in a_bound_x]
#print 'alpha boundary len', len(a_bound_x)

#normalize number of tie lines to match alpha
beta_interval = x_beta_max/(len(a_bound_x)+1)


b_bound_x = [beta_interval*i for i in xrange(1,number_tie+1)]
b_bound_y = [b_boundary(i) for i in b_bound_x]
#print 'beta boundary len', len(b_bound_x)
 
ab_x = zip(a_bound_x,b_bound_x)
ab_y = zip(a_bound_y,b_bound_y)

n1_beta = []
n1_alpha = []
m_tie = []
for x,y in zip(ab_x,ab_y):	 
    n1_alpha.append(x[0])
    n1_beta.append(x[1])
    tie_line = (y[1]-y[0])/(x[1]-x[0])
    m_tie.append(tie_line)
 
############## Master list of all sets in equilibrium n1_beta connected to n1_alpha by a tie line of a slope m_tie
f_beta = zip(n1_beta,n1_alpha,m_tie)
#f_beta_array = np.array(f_beta)
            
def get_equilibria(n1, n2):
    """returns equilibrium compositions for given point within 2 phase zone"""
    try:
        assert n1>0 and n1<1 
        assert 	n2>0 and n2<1 
        assert n1+n2<1
    except AssertionError:
        print 'Values out of phase diagram'
        return
    try:
        assert n2<a_boundary(n1)
        assert n2>b_boundary(n1)
    except AssertionError:
        print 'Point out of 2-phase zone'
        return
    for i in range(len(f_beta)+1):
        try:
            if f_beta[i][0]<n1 and f_beta[i+1][0]>n1:
                # mtie - tie line direction at node n1_neighbour closest to n1
                if abs(f_beta[i][0]-n1)<abs(f_beta[i+1][0]-n1):
                    mtie = f_beta[i][2]
                    n1_neighbour = f_beta[i][0]
                    n1_index = i
                else:
                    mtie = f_beta[i+1][2]
                    n1_neighbour = f_beta[i+1][0]
                    n1_index = i+1
#                print 'mtie at n1 neighbour:', mtie
#                print 'n1_neighbour', n1_neighbour
#                print 'n1 neighbour index', n1_index
                break
        except IndexError:
            print 'Index error'
            break
    #search for n1_beta corresponding to n1,n2  
    if mtie<0:
        tol = 10
        for j in f_beta[n1_index:]:
            mt = (b_boundary(j[0])-n2)/(j[0]-n1)
            if abs(j[2]-mt)<tol:
                tol = abs(j[2]-mt)
                tie_min_entry = j
                tie_min_index = f_beta.index(j)                
    elif mtie>0:
        tol = 10
        for j in f_beta[:n1_index]:
            mt = (b_boundary(j[0])-n2)/(j[0]-n1)
            if abs(j[2]-mt)<tol:
                tol = abs(j[2]-mt)
                tie_min_entry = j
                tie_min_index = f_beta.index(j)
#    print 'error of tie line direction', tol
    return tie_min_index, tie_min_entry 

def test_equilibria():
    """Testing function for get_equilibria"""
    print 'test case1: (0.15, 0.5)' 
    t_index, t_entry = get_equilibria(0.1, 0.5)
    print 'index: ', t_index, ', equilibrium at beta: ', t_entry
    print 'test case2: (0.5, 0.4)'
    t_index, t_entry = get_equilibria(0.5, 0.4)
    print 'index: ', t_index, ', equilibrium at beta: ', t_entry
    print 'test case3 (0.3, 0.5)'
    t_index, t_entry = get_equilibria(0.3, 0.5)
    print 'index: ', t_index, ', equilibrium at beta: ', t_entry




