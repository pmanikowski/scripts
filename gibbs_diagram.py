#!/usr/bin/python

import matplotlib.pyplot as plt
import numpy as np
import math
from multi_lists import *


def gibbs_diag(filename=''):
    x = np.linspace(0.0,1.0, num=grid+1)
    #plotting phase boundaries and hypertenuse
    plt.plot(x,y_roof, color='black')
    plt.plot(x[:x_beta_index+1],beta_bound, color='black')
    plt.plot(x[:x_alpha_index+1],alpha_bound, color='black')
    for x,y in zip(ab_x,ab_y)[::50]:
        plt.plot(x,y, color='black',linewidth=0.25 )    
    if filename !='':
        plt.savefig(filename+'.jpg')  
    plt.show()    
    return True

def gibbs_diagram_paths(a, b, filename='', t='line'):
    x = np.linspace(0.0,1.0, num=grid+1)
    #plotting phase boundaries and hypertenuse
    plt.plot(x,y_roof, color='black')
    plt.plot(x[:x_beta_index+1],beta_bound, color='black')
    plt.plot(x[:x_alpha_index+1],alpha_bound, color='black')
    for x,y in zip(ab_x,ab_y)[::50]:
        plt.plot(x,y, color='black',linewidth=0.25 )    
    #data for diffusion paths
    if t == 'line':    
        plt.plot(a,b)
    else:
        plt.plot(a,b, '.')
    if filename != '':
        plt.savefig(filename +'.jpg')  
    plt.show()
    return True

def gibbs_diagram_multipaths(a1, b1, a2, b2, a3, b3, filename=''):
    x = np.linspace(0.0,1.0, num=grid+1)
    #plotting phase boundaries and hypertenuse
    plt.plot(x,y_roof, color='black')
    plt.plot(x[:x_beta_index+1],beta_bound, color='black')
    plt.plot(x[:x_alpha_index+1],alpha_bound, color='black')
    for x,y in zip(ab_x,ab_y)[::50]:
        plt.plot(x,y, color='black',linewidth=0.25 )    
    #data for diffusion patgs    
    plt.plot(a1,b1, color = 'red')
    plt.plot(a2,b2, color = 'blue')
    plt.plot(a3,b3, color = 'green')
    
    if filename != '':
        plt.savefig(filename +'.jpg')  
    plt.show()    
    plt.close()    
    return True
